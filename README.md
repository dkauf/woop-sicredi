# Woop Sicredi


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

##### Aplicativo desenvolvida utilizando **Kotlin** permite visualizar a listagem de eventos através de um CardView na tela principal. Contém um recyclerview horizontal para visualizar logo dos eventos, como também a listagem das pressoas que irão se apresentar. Ao clicar no card acessa aos detalhes do evento onde é possível realizar o checkin no mesmo.
## Estrutura de pastas
```sh
└── app
    └── src
        └── br.com.dkauf.sicred
                          └── extension
                          └── interfaces
                          └── model
                          └── service
                          └── ui
                               └── activity
                               └── adapter
                               └── dialog
                               └── utils
```
### Bibliotecas


* [Databiding] - 3.1.4
* [Retrofit] - 2.4.0
* [RxJava] - 2.1.6
* [RxAndroid] - 2.0.1
* [glide] - 4.3.1
* [carousel] - 1.2.4


   [Databiding]: <https://developer.android.com/topic/libraries/data-binding/>
   [Retrofit]: <https://square.github.io/retrofit/t>
   [RxJava]: <https://github.com/ReactiveX/RxJava>
   [RxAndroid]: <https://github.com/ReactiveX/RxAndroid>
   [glide]: <https://github.com/bumptech/glide>
   [carousel]: <https://github.com/Azoft/CarouselLayoutManager>

### Funcionalidade

- listagem de eventos
    - scrool horizontal imagens
- detalhes do evento;
    - paralax
    - FAB button
- checkin
    - TextInputLayout