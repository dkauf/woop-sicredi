package br.com.dkauf.sicred.ui.utils

import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener


fun getCarouselLayoutManager(): CarouselLayoutManager {
    val carouselLayout = CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, false)
    carouselLayout.setPostLayoutListener(CarouselZoomPostLayoutListener())
    return carouselLayout
}