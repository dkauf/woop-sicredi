package br.com.dkauf.sicred.model

import junit.framework.Assert.assertEquals
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.instanceOf
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CheckinTest {

    val checkin = Checkin("1", "teste", "teste@gmail.com")

    @Test
    fun deve_validar_tipoDaClasse() {

        assertThat(checkin, instanceOf(Checkin::class.java))
    }

    @Test
    fun deve_validate_parametros() {

        assertEquals(checkin.name, "teste")
    }
}