package br.com.dkauf.sicred.extension

import android.widget.EditText
import br.com.dkauf.sicred.custom.EditableMock
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.RETURNS_MOCKS
import org.mockito.Mockito.mock
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class EditableExtensionTest {

    @Spy
    var textInputLayout = mock(EditText::class.java, RETURNS_MOCKS)

    @Test
    fun deve_retornarTrue_QuandoContemValor() {

        Mockito.`when`(textInputLayout.getText()).thenReturn(EditableMock("123"))

        assertTrue(textInputLayout.validateField())
    }

    @Test
    fun deve_retornarFalse_quandoNaoContemValor() {

        assertFalse(textInputLayout.validateField())
    }

    @Test
    fun deve_retornarTrue_quandoEmailValido() {
        val email = "test@gmail.com"

        Mockito.`when`(textInputLayout.getText()).thenReturn(EditableMock(email))
        assertTrue(textInputLayout.isValidEmail())
    }

    @Test
    fun deve_retornarFalse_quandoEmailInvalido() {
        Mockito.`when`(textInputLayout.getText()).thenReturn(EditableMock("@gmail.com"))
        assertFalse(textInputLayout.isValidEmail())

        Mockito.`when`(textInputLayout.getText()).thenReturn(EditableMock("gmail.com"))
        assertFalse(textInputLayout.isValidEmail())

        Mockito.`when`(textInputLayout.getText()).thenReturn(EditableMock("teste@gmail"))
        assertFalse(textInputLayout.isValidEmail())

        Mockito.`when`(textInputLayout.getText()).thenReturn(EditableMock("teste@.com"))
        assertFalse(textInputLayout.isValidEmail())

        Mockito.`when`(textInputLayout.getText()).thenReturn(EditableMock("teste@.com.br"))
        assertFalse(textInputLayout.isValidEmail())
    }
}