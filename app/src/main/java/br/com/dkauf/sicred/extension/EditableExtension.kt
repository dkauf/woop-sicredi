package br.com.dkauf.sicred.extension

import android.text.Editable
import android.widget.EditText
import br.com.dkauf.sicred.extension.Patterns.EMAIL_ADDRESS
import java.util.regex.Pattern

object Patterns {
    private const val EMAIL_PATTERN =
        ("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")

    val EMAIL_ADDRESS: Pattern = Pattern.compile(EMAIL_PATTERN)

}

fun EditText.validateField(): Boolean {

    if (isNullOrEmpty(text)) {
        setError("Campo Obrigatório")
        return false
    }
    return true
}

fun isNullOrEmpty(text: Editable) = text == null || text.length == 0

fun EditText.isValidEmail(): Boolean {
    val pattern = EMAIL_ADDRESS
    if( !pattern.matcher(this.text).matches()){
        setError("Digite um email válido")
        return false
    }
    return true
}
