package br.com.dkauf.sicred.interfaces

import br.com.dkauf.sicred.model.Events

interface OnClickItemListener {
    fun onItemClick(item: Events)
}
