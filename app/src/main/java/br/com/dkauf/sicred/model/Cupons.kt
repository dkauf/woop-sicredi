package br.com.dkauf.sicred.model

import java.io.Serializable

data class Cupons(val id: String?, val eventId: String?, val discount: Number?) : Serializable
