package br.com.dkauf.sicred.extension


import org.junit.Test

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class NumberExtensionTest {

    @Test
    fun deve_formatarParaMoeda_QuandoRecebeValorDouble() {

        assertThat<String>(200.0.formateCurrency(), `is`<String>(equalTo<String>("R$ 200,00")))
        assertThat<String>(2000.0.formateCurrency(), `is`<String>(equalTo<String>("R$ 2.000,00")))
        assertThat<String>(20000.0.formateCurrency(), `is`<String>(equalTo<String>("R$ 20.000,00")))
        assertThat<String>(200000.0.formateCurrency(), `is`<String>(equalTo<String>("R$ 200.000,00")))
        assertThat<String>(2000000.0.formateCurrency(), `is`<String>(equalTo<String>("R$ 2.000.000,00")))
        assertThat<String>(20000000.0.formateCurrency(), `is`<String>(equalTo<String>("R$ 20.000.000,00")))
    }

    @Test
    fun deve_formatarParaData_QuandoRecebeValorDouble() {

        assertThat<String>(1534784400000.formatDate(), `is`<String>(equalTo<String>("20/08/2018")))
    }
}