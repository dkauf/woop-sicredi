package br.com.dkauf.sicred.ui.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.dkauf.sicred.R
import br.com.dkauf.sicred.databinding.EventItemBinding
import br.com.dkauf.sicred.interfaces.Callback
import br.com.dkauf.sicred.interfaces.OnClickItemListener
import br.com.dkauf.sicred.model.Events
import br.com.dkauf.sicred.model.People
import br.com.dkauf.sicred.ui.utils.getCarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CenterScrollListener
import kotlinx.android.synthetic.main.event_item.view.*

class EventsAdapter(
    private val listEvents: List<Events>,
    private val itemListener: OnClickItemListener
) : RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.event_item, parent, false
            )
        )
    }

    override fun getItemCount(): Int = listEvents.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setData(listEvents[position], itemListener)
    }

    class ViewHolder(val binding: EventItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun setData(event: Events, itemListener: OnClickItemListener) {
            binding.model = event
            val carouselLayoutManager = getCarouselLayoutManager()

            event.people!!.addFirst(People(picture = event.image))

            createRecyclerView(carouselLayoutManager, event, itemListener)

            itemView.setOnClickListener {
                itemListener.onItemClick(event)
            }
        }

        private fun createRecyclerView(
            carouselLayoutManager: CarouselLayoutManager,
            event: Events,
            itemListener: OnClickItemListener
        ) {
            with(itemView.recyclerview_carousel) {
                layoutManager = carouselLayoutManager
                setHasFixedSize(false)
                adapter = InfoAdapter(
                    event.people!!,
                    object : Callback {
                        override fun done() {
                            itemListener.onItemClick(event)
                        }
                    }
                )
                addOnScrollListener(CenterScrollListener())
            }
        }
    }
}