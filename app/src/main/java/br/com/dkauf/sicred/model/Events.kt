package br.com.dkauf.sicred.model

import br.com.dkauf.sicred.extension.formatDate
import br.com.dkauf.sicred.extension.formateCurrency
import java.io.Serializable
import java.util.*

data class Events(
    val id: String?,
    val title: String?,
    val price: Number?,
    val latitude: String?,
    val longitude: String?,
    val image: String?,
    val description: String?,
    val date: Number?,
    val people: LinkedList<People>?,
    val cupons: List<Cupons>?
) : Serializable {

    fun getDate(): String {
        return date!!.formatDate()
    }

    fun getPrice(): String {
        return price!!.formateCurrency()
    }
}
