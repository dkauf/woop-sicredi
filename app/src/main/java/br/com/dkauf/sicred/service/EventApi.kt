package br.com.dkauf.sicred.service

import br.com.dkauf.sicred.model.Checkin
import br.com.dkauf.sicred.model.Events
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface EventApi {
    @GET("events")
    fun events(): Observable<List<Events>>

    @GET("events/{id}")
    fun eventDetails(@Path("id") id: String?): Observable<Events>

    @POST("checkin")
    fun checkin(@Body checking: Checkin): Observable<Events>
}
