package br.com.dkauf.sicred.model

import android.text.Editable
import java.io.Serializable

data class Checkin(
    var eventId: String? = "",
    val name: String? = "",
    val email: String?
) : Serializable