package br.com.dkauf.sicred.ui.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.dkauf.sicred.R
import br.com.dkauf.sicred.databinding.ImageDescriptionBinding
import br.com.dkauf.sicred.interfaces.Callback
import br.com.dkauf.sicred.model.People

class InfoAdapter(private val listPeople: List<People>, private val callback: Callback?) :
    RecyclerView.Adapter<InfoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.image_description, parent, false
            )
        )
    }

    override fun getItemCount(): Int = listPeople.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setData(listPeople[position], callback!!)
    }

    class ViewHolder(val binding: ImageDescriptionBinding) : RecyclerView.ViewHolder(binding.root) {

        fun setData(people: People, callback: Callback?) {
            binding.model = people
            itemView.setOnClickListener {
                callback!!.done()
            }
        }
    }
}