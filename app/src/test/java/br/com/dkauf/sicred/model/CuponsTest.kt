package br.com.dkauf.sicred.model

import junit.framework.Assert.assertEquals
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers
import org.junit.Test

class CuponsTest {

    val cupons = Cupons("1", "1", 5.00)

    @Test
    fun deve_validar_tipoDaClasse() {

        assertThat(cupons, Matchers.instanceOf(Cupons::class.java))
    }

    @Test
    fun deve_validate_parametros() {

        assertEquals(cupons.eventId, "1")
    }
}