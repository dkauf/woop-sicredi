package br.com.dkauf.sicred.extension

import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

fun Number.formateCurrency(): String {
    val brazilianFormat = DecimalFormat
        .getCurrencyInstance(Locale("pt", "br"))

    return brazilianFormat.format(this)
}

fun Number.formatDate(): String {
    val sdf = SimpleDateFormat("dd/MM/yyy", Locale.getDefault())

    return sdf.format(this)
}