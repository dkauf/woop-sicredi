package br.com.dkauf.sicred.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import br.com.dkauf.sicred.R
import br.com.dkauf.sicred.interfaces.OnClickItemListener
import br.com.dkauf.sicred.model.Events
import br.com.dkauf.sicred.service.RetrofitInitializer
import br.com.dkauf.sicred.ui.adapter.EventsAdapter
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    companion object {
        const val INTENT_STRING = "item"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        callEvents()
    }

    private fun callEvents() {
        RetrofitInitializer().listEvents().events()
                .subscribeOn(Schedulers.newThread())
                .observeOn(mainThread())
                .subscribe(this::sucess, this::error)
    }

    private fun sucess(listEvents: List<Events>) {
        createRecyclerView(listEvents)
    }

    private fun error(error: Throwable) {
        Snackbar.make(
                cordinator_layout,
                R.string.msg_error_events,
                Snackbar.LENGTH_LONG
        ).setAction(R.string.title_retry, {
            callEvents()
        }).show()
    }

    private fun createRecyclerView(listResult: List<Events>?) {
        with(recyclerview) {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = getAdapter(listResult)
        }
    }

    private fun getAdapter(listEvents: List<Events>?): EventsAdapter {
        return EventsAdapter(listEvents!!, object : OnClickItemListener {
            override fun onItemClick(item: Events) {

                val intent = Intent(this@MainActivity, DetailsActivity::class.java)

                intent.putExtra(INTENT_STRING, item)
                startActivity(intent)
            }
        })
    }
}
