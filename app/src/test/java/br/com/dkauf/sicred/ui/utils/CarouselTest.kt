package br.com.dkauf.sicred.ui.utils

import junit.framework.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class CarouselTest {

    @Test
    fun deve_retornar_InstanciaDeCarousel() {

        val carousel = getCarouselLayoutManager()

        assertNotNull(carousel)
    }
}