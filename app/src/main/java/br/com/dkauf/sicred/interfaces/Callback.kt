package br.com.dkauf.sicred.interfaces

interface Callback {
    fun done()
}