package br.com.dkauf.sicred.ui.dialog

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import br.com.dkauf.sicred.R
import br.com.dkauf.sicred.extension.isValidEmail
import br.com.dkauf.sicred.extension.validateField
import br.com.dkauf.sicred.interfaces.Callback
import br.com.dkauf.sicred.model.Checkin
import br.com.dkauf.sicred.service.RetrofitInitializer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_checkin.view.*

class CustomDialog(
    context: Context?,
    val eventId: String,
    callback: Callback
) : AlertDialog.Builder(context) {

    init {

        setTitle(R.string.dialog_tittle)

        val view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_checkin, null)
        setView(view)

        val dialog = this.create()
        dialog.show()

        with(view.btn_send) {
            setOnClickListener {

                val checkin = getCheckinFields(view)

                if (checkin != null) {
                    callCheckin(checkin, callback, dialog)
                }
            }
        }
    }

    private fun callCheckin(
        checkin: Checkin?,
        callback: Callback,
        dialog: AlertDialog
    ) {
        RetrofitInitializer().listEvents().checkin(checkin!!)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                callback.done()
                dialog.dismiss()
            }
    }

    private fun getCheckinFields(view: View): Checkin? {
        var email = ""
        var name = ""
        with(view.til_email.editText!!) {
            if (validateField() && isValidEmail()) {
                email = text.toString()
            }
        }

        with(view.til_user.editText!!) {
            if (validateField()) {
                name = text.toString()
            }
        }
        if (name.isNotEmpty() && email.isNotEmpty()) {
            return Checkin(eventId, email, name)
        }

        return null
    }
}