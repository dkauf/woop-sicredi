package br.com.dkauf.sicred.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import br.com.dkauf.sicred.R
import br.com.dkauf.sicred.interfaces.Callback
import br.com.dkauf.sicred.model.Events
import br.com.dkauf.sicred.model.People
import br.com.dkauf.sicred.service.RetrofitInitializer
import br.com.dkauf.sicred.ui.adapter.InfoAdapter
import br.com.dkauf.sicred.ui.dialog.CustomDialog
import br.com.dkauf.sicred.ui.utils.getCarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselLayoutManager
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_details.*

class DetailsActivity : AppCompatActivity() {

    private lateinit var events: Events

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)

        events = getIntent(intent)

        createFabButton(events)

        callSingleEvent(events)
    }

    private fun callSingleEvent(events: Events): Disposable? {
        return RetrofitInitializer().listEvents().eventDetails(events.id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::sucess, this::error)
    }

    private fun sucess(events: Events?) {
        populateUI(events!!)
    }

    private fun error(error: Throwable) {
        Snackbar.make(
            cordinator_layout,
            R.string.msg_error_single_event,
            Snackbar.LENGTH_LONG
        ).setAction(R.string.title_retry, {
            callSingleEvent(events)
        }).show()
    }

    private fun createFabButton(events: Events) {
        fab.setOnClickListener { view ->
            CustomDialog(this, events.id!!, object : Callback {
                override fun done() {
                    Snackbar.make(
                        view,
                        R.string.msg_sucess_checkin,
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            })
        }
    }

    private fun populateUI(it: Events) {

        toolbar_layout.setTitle(it.title)
        txt_title.text = it.title
        txt_description.text = it.description

        val carouselLayoutManager = getCarouselLayoutManager()

        it.people!!.addFirst(People(picture = it.image))
        createRecyclerView(carouselLayoutManager, it)
    }

    fun getIntent(intent: Intent): Events {
        return intent.getSerializableExtra(MainActivity.INTENT_STRING) as Events
    }

    private fun createRecyclerView(
        carouselLayoutManager: CarouselLayoutManager, event: Events
    ) {
        with(recyclerview_carousel) {
            layoutManager = carouselLayoutManager
            setHasFixedSize(false)
            adapter = InfoAdapter(event.people!!, object : Callback {
                override fun done() {}
            })
            addOnScrollListener(com.azoft.carousellayoutmanager.CenterScrollListener())
        }
    }
}
