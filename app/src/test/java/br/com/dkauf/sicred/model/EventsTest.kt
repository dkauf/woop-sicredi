package br.com.dkauf.sicred.model

import br.com.dkauf.sicred.extension.formateCurrency
import junit.framework.Assert
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class EventsTest {

    val events = Events("1", "teste", 100.0, "1", "1", "path", "teste", 1534784400000, null, null)

    @Test
    fun deve_validar_tipoDaClasse() {

        assertThat(events, Matchers.instanceOf(Events::class.java))
    }

    @Test
    fun deve_validate_parametros() {

        Assert.assertEquals(events.title, "teste")
    }

    @Test
    fun deve_formatarParaMoeda_QuandoRecebeValorDouble() {

        assertThat(events.price?.formateCurrency(), `is`(equalTo("R$ 100,00")))
    }
}
