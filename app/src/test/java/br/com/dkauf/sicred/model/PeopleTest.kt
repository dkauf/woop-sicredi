package br.com.dkauf.sicred.model

import junit.framework.Assert
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.Test

class PeopleTest {

    val people = People("1", "1", "teste", "path")

    @Test
    fun deve_validar_tipoDaClasse() {

        MatcherAssert.assertThat(people, Matchers.instanceOf(People::class.java))
    }

    @Test
    fun deve_validate_parametros() {

        Assert.assertEquals(people.name, "teste")
    }
}