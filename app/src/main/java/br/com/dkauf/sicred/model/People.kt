package br.com.dkauf.sicred.model

import java.io.Serializable

data class People(
    val id: String? = "",
    val eventId: String? = "",
    val name: String? = "",
    val picture: String?
) : Serializable